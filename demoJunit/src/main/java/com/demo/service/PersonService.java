package com.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.entity.Person;
import com.demo.repo.PersonRepo;

@Service
public class PersonService {
	@Autowired
	private PersonRepo persRepo;
	
	public List<Person> getAllPerson(){
		return this.persRepo.findAll();
	}

	public PersonService(PersonRepo persRepo) {
		super();
		// TODO Auto-generated constructor stub
		this.persRepo=persRepo;
	}
	
	
}

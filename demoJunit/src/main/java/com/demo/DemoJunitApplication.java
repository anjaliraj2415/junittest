package com.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoJunitApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoJunitApplication.class, args);
		System.out.println("Testing");
	}

}
